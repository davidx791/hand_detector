#CONVERT IMAGE TO CONTOURS

import numpy as np
import cv2

#example iamge from camera or file
cap = cv2.VideoCapture(0)
cv2.waitKey(1000)
ret, im = cap.read()
#im = cv2.imread('C:/Users/david/Desktop/1.png')

#convert image
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(imgray,80,255,0)
_,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(im,contours,-1,(0,0,255),1)

#show image
cv2.imshow('image',im)
cv2.waitKey(0)
cv2.destroyAllWindows()
