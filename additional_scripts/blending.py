#BLENDING OF TWO IMAGES
import cv2

#example images
img1 = cv2.imread('C:/Users/david/Desktop/3.png')
img2 = cv2.imread('C:/Users/david/Desktop/4.png')

#blending with proportion 0.35:0.65
dst = cv2.addWeighted(img1,0.35,img2,0.65,0)
cv2.imshow('dst',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
