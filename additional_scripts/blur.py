#MAKING BLUR ON EXAMPLE IMAGE

import cv2
import numpy

# example image
src = cv2.imread('C:/Users/david/Desktop/blur.png', cv2.IMREAD_UNCHANGED)
 
# apply guassian blur on src image
dst = cv2.GaussianBlur(src,(5,5),cv2.BORDER_DEFAULT)
 
# display input and output image
cv2.imshow("Gaussian Smoothing",numpy.hstack((src, dst)))
cv2.waitKey(0) #waits on a pressing key
cv2.destroyAllWindows()
