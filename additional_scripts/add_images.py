import cv2
import freenect
import numpy as np
import cv2.aruco as aruco

def get_rgb():
    array,_ = freenect.sync_get_video()
    array=cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    return array

#funkcja zwracająca klatke głębi z kinecta
def get_depth(array):
    np.clip(array, 0, 2047, array)
    array >>= 3
    array = array.astype(np.uint8)
    return array

#funkcja zwracająca klatke głębi z kinecta z milimetrach
def get_depth_mm():
    array,_=freenect.sync_get_depth(0,5)
    return array


Mat2_3 = np.array([[9.17944405e-01 , 2.82464091e-02, -3.43207816e+00], [ 1.73282722e-04 , 9.24796632e-01 , 3.55746281e+01]])

if __name__ == "__main__":
    
    while 1:

        #***********POBIERANIE KLATEK*************#
        rgb = get_rgb()
                
        depth_mm=get_depth_mm()
        depth=get_depth(depth_mm.copy())

        depth2=cv2.cvtColor(depth,cv2.COLOR_GRAY2BGR)
        add2 = cv2.addWeighted(depth2,0.5,rgb,0.5,0.0)
        cv2.imshow('Nalozenie domyslne',add2)


        depth=cv2.warpAffine(depth,Mat2_3,(640,480) )
        depth=cv2.cvtColor(depth,cv2.COLOR_GRAY2BGR)       

        #***********NAKLADANIE OBRAZOW*************#
        add = cv2.addWeighted(depth,0.5,rgb,0.5,0.0)
        cv2.imshow('Nalozenie poprawne',add)

        #wyjscie z pętli
        k=cv2.waitKey(5) & 0xFF
        if k==27:
            break

    freenect.sync_stop()
    cv2.destroyAllWindows()  
