import numpy as np
import cv2
import glob

#calibration images from two cameras

def read_images(imgleftpath, imgrightpath):
    imgleft  = cv2.imread(imgleftpath)
    imgright = cv2.imread(imgrightpath)

    print imgright.shape[:2]
    return imgleft, imgright


def show_images(imgleft, imgright):
    cv2.imshow('left',  imgleft)
    cv2.imshow('right', imgright)


criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
objp = np.zeros((6*7,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

objpoints  = [] # 3d point in real world space

img_size = 0

left_imgpoints  = [] # 2d points in image plane.
right_imgpoints = [] # 2d points in image plane.

leftcamerimages =  glob.glob('img2/left*.jpg')
rightcamerimages = glob.glob('img2/right*.jpg')

print 'tuple size'
print len(leftcamerimages)

for i, name in enumerate(leftcamerimages):
    imgl, imgr = read_images(leftcamerimages[i], rightcamerimages[i])

    print 'Size of fig '
    print imgl.shape[::-2]
    
    imglgray = cv2.cvtColor(imgl, cv2.COLOR_BGR2GRAY)
    imgrgray = cv2.cvtColor(imgr, cv2.COLOR_BGR2GRAY)

    retl, cornersl = cv2.findChessboardCorners(imglgray, (7,6), None)
    retr, cornersr = cv2.findChessboardCorners(imgrgray, (7,6), None)


    if retl == True and retr == True:
        print "there are corners"
        objpoints.append(objp)
        corners2left = cv2.cornerSubPix(imglgray, cornersl, (11, 11), (-1, -1), criteria)
        corners2right = cv2.cornerSubPix(imgrgray, cornersr, (11, 11), (-1, -1), criteria)

        left_imgpoints.append(cornersl)
        right_imgpoints.append(cornersr)

        cv2.drawChessboardCorners(imglgray, (7, 6), corners2left, retl)
        cv2.drawChessboardCorners(imgrgray, (7, 6), corners2right, retr)

        cv2.imshow('imglgray', imglgray)
        cv2.imshow('imgrgray', imgrgray)
        cv2.waitKey(500)


    img_size = imglgray.shape[::-1]

left_rt, left_M1, left_d1, left_r1, left_t1 = cv2.calibrateCamera(objpoints, left_imgpoints, img_size, None, None)
right_rt, right_M1, right_d1, right_r1, right_t1 = cv2.calibrateCamera(objpoints, right_imgpoints, img_size, None, None)

print left_rt
print left_M1
print left_d1
print left_r1
print left_t1

flags = 0
flags |= cv2.CALIB_FIX_INTRINSIC
    # flags |= cv2.CALIB_FIX_PRINCIPAL_POINT
flags |= cv2.CALIB_USE_INTRINSIC_GUESS
flags |= cv2.CALIB_FIX_FOCAL_LENGTH
    # flags |= cv2.CALIB_FIX_ASPECT_RATIO
flags |= cv2.CALIB_ZERO_TANGENT_DIST
    # flags |= cv2.CALIB_RATIONAL_MODEL
   # flags |= cv2.CALIB_SAME_FOCAL_LENGTH
    # flags |= cv2.CALIB_FIX_K3
    # flags |= cv2.CALIB_FIX_K4
    # flags |= cv2.CALIB_FIX_K5

stereocalib_criteria = (cv2.TERM_CRITERIA_MAX_ITER +
                            cv2.TERM_CRITERIA_EPS, 100, 1e-5)

ret, M1, d1, M2, d2, R, T, E, F = cv2.stereoCalibrate(
        objpoints, left_imgpoints,
        right_imgpoints, left_M1, left_d1, right_M1, right_d1, img_size,
        criteria=stereocalib_criteria, flags=flags)

print('Intrinsic_mtx_left', M1)
print('dist_left', d1)
print('Intrinsic_mtx_right', M2)
print('dist_right', d2)
print('R', R)
print('T', T)
print('E', E)
print('F', F)

    # for i in range(len(self.r1)):
    #     print("--- pose[", i+1, "] ---")
    #     self.ext1, _ = cv2.Rodrigues(self.r1[i])
    #     self.ext2, _ = cv2.Rodrigues(self.r2[i])
    #     print('Ext1', self.ext1)
    #     print('Ext2', self.ext2)

print('')

camera_model = dict([('M1', M1), ('M2', M2), ('dist1', d1),
                         ('dist2', d2), ('rvecs1', left_r1),
                         ('rvecs2', right_r1), ('R', R), ('T', T),
                         ('E', E), ('F', F)])


print objp
cv2.waitKey(0)
cv2.destroyAllWindows()
