import cv2
import freenect
import numpy as np
import cv2.aruco as aruco
import math

#DEKLARACJA STALYCH
dictionary = aruco.getPredefinedDictionary(aruco.DICT_6X6_250)
cameraMatrix=np.array([[521.22912511 ,  0.   ,      311.13641898], [  0.    ,     521.337171  , 255.51874882], [  0.   ,        0.      ,     1.        ]],dtype=float)
distCoeffs=np.array([2.13103104e-01, -6.45518070e-01, -1.80314531e-03 , 2.52357949e-04 ,   6.24542850e-01],dtype=float)
cameraMatrixD=np.array([[586.47442685 ,  0.      ,   311.75929388], [  0.   ,      586.37975653 ,246.56039078], [  0. ,          0.  ,         1.        ]],dtype=float)
distCoeffsD=np.array([-0.13459809 , 0.56052854 , 0.0025496 , -0.00434469 ,-0.75248939],dtype=float)

colorList = [[0,0,255],[0,127,255],[0,255,255],[0,255,127],[127,255,0],[255,255,0],[255,0,127],[255,0,0],[255,0,127],[255,0,255]]
fx=cameraMatrix[0][0]
fy=cameraMatrix[1][1]
cx=cameraMatrix[0][2]
cy=cameraMatrix[1][2]
I=np.array([[1,0,0],[0,1,0],[0,0,1]],dtype=float)
HAND_AXIS_SIZE=0.07
MAIN_AXIS_SIZE=0.07
AXIS_SIZE=0.02
idsList=[15,22,37,48,52]
paper_idsList=[12,25,30,40,43]
rvecsTo69=np.array([[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]])
tvecsTo69=np.array([[-0.038,0.0965,0],[0.077,0.088,0],[0.100,-0.061,0],[0.0855,-0.122,0],[-0.022,-0.1085,0]])
rvecsTo70=np.array([[0,-1.5708,0],[-1.5708,0,0],[3.14159,0,0],[1.5708,0,0],[0,1.5708,0]])
tvecsTo70=np.array([[-0.0285,0,-0.0285],[0,0.0285,-0.0285],[0,0,-0.057],[0,-0.0285,-0.0285],[0.0285,0,-0.0285]])

#DEKLARACJA ZMIENNYCH
camera_position=np.array([[0,0,0],[0,0,0],[0,0,0]],dtype=float)
hand_position=np.array([[0,0,0],[0,0,0],[0,0,0]],dtype=float)
prev_hand_rvec=np.array([0,0,0],dtype=float)
tmpA=0
centerList_g=[]
centerList_d=[]
tmp_farList=[]
change=0
licz=0
dl_aruco_hand=0
hand_lack=0
found =0
paper_found=0
found_id =0
paper_found_id=0
found=0
paper_found=0

tmp_R=np.array(['-','-','-'])
tmp_T=np.array(['-','-','-'])

tmp_R2=np.array([0,0,0],dtype=float)
tmp_T2=np.array([0,0,0],dtype=float)

tvecList=[]
rvecsList=[]
main_rvec=np.array([0,0,0],dtype=float)
main_tvec=np.array([0,0,0],dtype=float)
main_R=np.array([[0,0,0],[0,0,0],[0,0,0]],dtype=float)
main_A=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,1]],dtype=float)

hand_R=np.array([[0,0,0],[0,0,0],[0,0,0]],dtype=float)
hand_A=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,1]],dtype=float)

#zmienne dla kostki aruco 
R=np.array([[0,0,0],[0,0,0],[0,0,0]],dtype=float)
A=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,1]],dtype=float)
inv_A2=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,1]],dtype=float)

#zmienne dla kartki aruco 
paper_R=np.array([[0,0,0],[0,0,0],[0,0,0]],dtype=float)
paper_A=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,1]],dtype=float)
paper_inv_A2=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,1]],dtype=float)

#DEKLARACJA FUNKCJI
#funkcja zwracająca klatkę rgb z kinecta
def get_rgb():
    array,_ = freenect.sync_get_video()
    array=cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    return array

#funkcja zwracająca klatke głębi z kinecta
def get_depth(array):
    np.clip(array, 0, 2047, array)
    array >>= 3
    array = array.astype(np.uint8)
    return array

#funkcja zwracająca klatke głębi z kinecta z milimetrach
def get_depth_mm():
    array,_=freenect.sync_get_depth(0,5)
    return array

#funkcja rysująca ślad środka dłoni
def drawPoints(image,centerList):
    n=len(centerList)
    if n>10:
        n=10        
    for i in range(0,n): 
        cv2.circle(image,centerList[i],7,colorList[i],-1)

#funkcja zwracająca odwrotność macierzy rotacji
def inv_R(tmp_rvec):
    tmp_R=np.array([[0,0,0],[0,0,0],[0,0,0]],dtype=float)
    if tmp_rvec[0]==0 and tmp_rvec[1]==0 and tmp_rvec[2]==0:
        tmp_R=I
    else:
        cv2.Rodrigues(tmp_rvec,tmp_R)
    tmp_R_tran=np.transpose(tmp_R)
    return tmp_R_tran

#funkcja zwracająca odwrotność macierzy translacji
def inv_T(tmp_rvec,tmp_tvec):
    tmp_R_tran=inv_R(tmp_rvec)
    tmp_T=np.array([[tmp_tvec[0]],[tmp_tvec[1]],[tmp_tvec[2]]],dtype=float)
    tmp_T_minusRT=np.dot(np.dot((-1)*np.eye(3),tmp_R_tran),tmp_T)
    return tmp_T_minusRT    

#rozpoznawanie dłoni
hand_cascade = cv2.CascadeClassifier('Hand_haar_cascade.xml')
foreground = cv2.bgsegm.createBackgroundSubtractorGSOC()

#nakładanie obrazow
Mat2_3 = np.array([[9.17944405e-01 , 2.82464091e-02, -3.43207816e+00], [ 1.73282722e-04 , 9.24796632e-01 , 3.55746281e+01]])

if __name__ == "__main__":
    
    while 1:

        #***********POBIERANIE KLATEK*************#
        rgb = get_rgb()
        depth_mm=get_depth_mm()
        depth=get_depth(depth_mm.copy())

        depth_3= cv2.warpAffine(depth,Mat2_3,(640,480) )
        depth_3=cv2.cvtColor(depth_3,cv2.COLOR_GRAY2BGR)       

        depth= cv2.warpAffine(depth,Mat2_3,(640,480) )
        
        #***********NAKLADANIE OBRAZOW*************#
        #add = cv2.addWeighted(depth_3,0.5,rgb,0.5,0.0)
        #add2 = cv2.addWeighted(depth_test,0.5,rgb,0.5,0.0)
        #add_test = np.hstack((add2,add))
        #cv2.imshow('ADD',add_test)
        
        mainWindow = np.zeros((600,960,1), np.uint8)
        mainWindow = cv2.cvtColor(mainWindow,cv2.COLOR_GRAY2RGB)
                          
        #***********WYKRYWANIE MARKEROW**********#
        corners,ids,ret=aruco.detectMarkers(rgb,dictionary)
        
        #********RYSOWANIE MARKEROW*********#
        n=len(corners)
        if n > 0:
            aruco.drawDetectedMarkers(rgb, corners,ids)
            rvecList,tvecList,_=aruco.estimatePoseSingleMarkers(corners,0.055,cameraMatrix,distCoeffs)
        
        #*************MARKERY*******************#
        draw_id=0
        aruco_change=0
        
        for i in range(0,n):#zapobieganie zmiany markera pomocniczego
            if found_id == ids[i]:
                found=1
            if paper_found_id == ids[i]:
                paper_found=1
            
        if n > 0:
            for i in range(0,n): 
                if ids[i] != 70 and ids[i] != 69 and ids[i] != 50: #***rysowanie innych markerów procz głownych*******#
                    aruco.drawAxis(rgb,cameraMatrix,distCoeffs,rvecList[i],tvecList[i],AXIS_SIZE)
                    
            for i in range(0,n):  #MARKERY NA KOSTCE#   
                if ids[i] == 70 and aruco_change==0:
                    main_rvec=np.array([rvecList[i][0][0],rvecList[i][0][1],rvecList[i][0][2]])
                    main_tvec=np.array([tvecList[i][0][0],tvecList[i][0][1],tvecList[i][0][2]])
                    aruco.drawAxis(rgb,cameraMatrix,distCoeffs,main_rvec,main_tvec,MAIN_AXIS_SIZE)
                    aruco_change=1
                    draw_id=70
                    
            for i in range(0,n):
                if aruco_change==0:
                    if ids[i] != 70 and ids[i]!=12  and ids[i]!= 25  and ids[i]!= 30 and ids[i]!=40 and ids[i]!= 43 and ids[i]!=69 and ids[i]!=50:
                        rvec=np.array([rvecList[i][0][0],rvecList[i][0][1],rvecList[i][0][2]])
                        tvec=np.array([tvecList[i][0][0],tvecList[i][0][1],tvecList[i][0][2]])
                        cv2.Rodrigues(rvec,R)
                        T=np.array([[tvec[0]],[tvec[1]],[tvec[2]]],dtype=float)
                        A[:3,:3]=R 
                        A[:3,3:4]=T

                        if found == 1:
                            for j in range(0,5):
                                if ids[i]==idsList[j]:
                                    inv_A2[:3,:3]=inv_R(rvecsTo70[j])
                                    inv_A2[:3,3:4]=inv_T(rvecsTo70[j],tvecsTo70[j])
                                    break
                        else:
                            for j in range(0,5):
                                if ids[i]==idsList[j]:
                                    inv_A2[:3,:3]=inv_R(rvecsTo70[j])
                                    inv_A2[:3,3:4]=inv_T(rvecsTo70[j],tvecsTo70[j])
                                    found_id=ids[i]
                                    break
                                            
                        main_A=np.dot(A,inv_A2)
                        main_R=main_A[:3,:3]
                        main_T=main_A[:3,3:4]
                                       
                        cv2.Rodrigues(main_R,main_rvec)
                        for j in range(0,3):
                            main_tvec[j]=main_T[j][0]
                            
                        aruco.drawAxis(rgb,cameraMatrix,distCoeffs,main_rvec,main_tvec,MAIN_AXIS_SIZE)
                        for j in range(0,n):
                            if ids[j] == 69: #***dorysowanie niewykorzystanego markera głownego *******#
                                aruco.drawAxis(rgb,cameraMatrix,distCoeffs,rvecList[j],tvecList[j],AXIS_SIZE)
                                break
                        aruco_change=1
                        draw_id=70
                        
            for i in range(0,n):   #MARKERY NA KARTCE#   
                if ids[i] == 69 and aruco_change==0:
                    main_rvec=np.array([rvecList[i][0][0],rvecList[i][0][1],rvecList[i][0][2]])
                    main_tvec=np.array([tvecList[i][0][0],tvecList[i][0][1],tvecList[i][0][2]])
                    aruco.drawAxis(rgb,cameraMatrix,distCoeffs,main_rvec,main_tvec,MAIN_AXIS_SIZE)
                    aruco_change=1
                    draw_id=69
                                       
            for i in range(0,n):
                if aruco_change==0:
                    if ids[i] != 69 and ids[i] != 15 and ids[i]!=22 and ids[i]!=37 and ids[i]!=48 and ids[i]!=52 and ids[i]!=70 and ids[i]!=50:
                        paper_rvec=np.array([rvecList[i][0][0],rvecList[i][0][1],rvecList[i][0][2]])
                        paper_tvec=np.array([tvecList[i][0][0],tvecList[i][0][1],tvecList[i][0][2]])
                        cv2.Rodrigues(paper_rvec,paper_R)
                        paper_T=np.array([[paper_tvec[0]],[paper_tvec[1]],[paper_tvec[2]]],dtype=float)
                        paper_A[:3,:3]=paper_R 
                        paper_A[:3,3:4]=paper_T

                        if paper_found == 1:
                            for j in range(0,5):
                                if ids[i]==paper_idsList[j]:
                                    paper_inv_A2[:3,:3]=inv_R(rvecsTo69[j])
                                    paper_inv_A2[:3,3:4]=inv_T(rvecsTo69[j],tvecsTo69[j])
                                    break
                        else:
                            for j in range(0,5):
                                if ids[i]==paper_idsList[j]:
                                    paper_inv_A2[:3,:3]=inv_R(rvecsTo69[j])
                                    paper_inv_A2[:3,3:4]=inv_T(rvecsTo69[j],tvecsTo69[j])
                                    paper_found_id=ids[i]
                                    break
                                
                        main_A=np.dot(paper_A,paper_inv_A2)
                        main_R=main_A[:3,:3]
                        main_T=main_A[:3,3:4]
                                       
                        cv2.Rodrigues(main_R,main_rvec)
                        for j in range(0,3):
                            main_tvec[j]=main_T[j]
                        aruco.drawAxis(rgb,cameraMatrix,distCoeffs,main_rvec,main_tvec,MAIN_AXIS_SIZE)
                        aruco_change=1
                        draw_id=69
           
        #Okreslenie pozycji dloni wykorzystujac marker#
        hand_change=0
        if n > 0:
            for i in range(0,n): 
                if ids[i] == 50: #***rysowanie markera dloni*******#
                    hand_rvec=np.array([rvecList[i][0][0],rvecList[i][0][1],rvecList[i][0][2]])
                    hand_tvec=np.array([tvecList[i][0][0],tvecList[i][0][1],tvecList[i][0][2]])
                    aruco.drawAxis(rgb,cameraMatrix,distCoeffs,hand_rvec,hand_tvec,HAND_AXIS_SIZE)
                    hand_change=1
                    break

        #*************POZYCJA DLONI*******************#
        #********PROGOWANIE GŁĘBI***********#
        depth_thresh=depth.copy()
        depth_thresh[np.where(depth_thresh==0)] = 255
        depth_thresh = cv2.medianBlur(depth_thresh,5)
        _,depth_thresh = cv2.threshold(depth_thresh, 254,255,cv2.THRESH_BINARY_INV)
          
        hand_d = hand_cascade.detectMultiScale(depth_thresh, 1.3, 5)
        mask_d = np.zeros(depth_thresh.shape, dtype = "uint8")
        for (x_d,y_d,w_d,h_d) in hand_d:
            a_d=x_d+w_d
            b_d=y_d+h_d
            if(x_d>=20):
                x_d=x_d-20
            else:
                x_d=0
            if(y_d>=20):
                y_d=y_d-20
            else:
                y_d=0
            if(a_d<=620):
                a_d=a_d+20
            else:
                a_d=640
            if(b_d<=460):
                b_d=b_d+20
            else:
                b_d=480
           # cv2.rectangle(depth,(x_d,y_d),(a_d,b_d), (0,0,0), 2)
            cv2.rectangle(mask_d, (x_d,y_d),(a_d,b_d),(255,255,255),-1)
        track_depth = cv2.bitwise_and(depth_thresh, mask_d)
        track_depth = cv2.GaussianBlur(track_depth,(7,7),0)

        #Znajdowanie konturow obrazu zawierajacego dlon
        _,contours_d,hierarchy_d = cv2.findContours(track_depth.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(depth, contours_d, 0, (0,0,0), 2)
        cv2.drawContours(rgb, contours_d, 0, (0,255,0), 0)

        tmpA=1
        if len(contours_d) > 0 and hand_change==0:
            cnt_d = max(contours_d, key = lambda d: cv2.contourArea(d))
            hull_d = cv2.convexHull(cnt_d)
            #cv2.drawContours(depth, [hull_d], 0,(0, 0, 0), 2)
            hull_d = cv2.convexHull(cnt_d, returnPoints=False)
            defects_d=cv2.convexityDefects(cnt_d,hull_d)
            far_d = []
            for i in range(defects_d.shape[0]):
                s,e,f,d = defects_d[i,0]
                start = tuple(cnt_d[s][0])
                end = tuple(cnt_d[e][0])
                far = tuple(cnt_d[f][0])

                #odleglosc punktu wglębienia od konturu
                if start[0]-end[0]!=0:
                    a_kier=1.00*(start[1]-end[1])/(start[0]-end[0]) #z rownania prostej
                    b_kier=start[1]-a_kier*start[0]
                    dist=abs(a_kier*far[0]-far[1]+b_kier)/math.sqrt(a_kier*a_kier+1) #odleglosc
                    if dist > 30: 
                        far_tmp=np.float32([start,end,far])
                        far_d.append(far_tmp)
                        #cv2.circle(rgb,far,6,[127,0,255],-1)
                        #cv2.circle(depth,far,6,[0,0,0],-1)
                
            #peak_x=cnt_d[0][0][0]
            #peak_y=cnt_d[0][0][1]
            #cv2.circle(depth,(peak_x,peak_y),10,[0,0,0],0)
            moments_d = cv2.moments(cnt_d)
            if moments_d['m00']!=0:
                center_d_x = int(moments_d['m10']/moments_d['m00']) # cx = M10/M00
                center_d_y = int(moments_d['m01']/moments_d['m00']) # cy = M01/M00
            center_d=(center_d_x,center_d_y)
            centerList_d.insert(0,center_d)
            tmpA=0
            center_d_z=depth_mm[center_d_y][center_d_x]/1000.0
            
            if center_d_z != 0:
                dl_aruco_hand=(-100)*center_d_z+120
            center_d_x=center_d_z*(center_d_x-cx)/fx
            center_d_y=center_d_z*(center_d_y-cy)/fy
            #print(center_d_x,' m ',center_d_y,' m ',center_d_z,' m ')
            
            
            if len(far_d) == 4  :
                center_thumb_d=(0,0)
                #for i in range(0,len(far_d)):
                    #cv2.circle(rgb,(far_d[i][2][0],far_d[i][2][1]),8,[127,0,255],0)

                dist_0_3=math.sqrt(abs(far_d[3][2][0]-far_d[0][2][0])*abs(far_d[3][2][0]-far_d[0][2][0])+abs(far_d[3][2][1]-far_d[0][2][1])*abs(far_d[3][2][1]-far_d[0][2][1]))
                dist_0_1=math.sqrt(abs(far_d[1][2][0]-far_d[0][2][0])*abs(far_d[1][2][0]-far_d[0][2][0])+abs(far_d[1][2][1]-far_d[0][2][1])*abs(far_d[1][2][1]-far_d[0][2][1]))
                dist_1_2=math.sqrt(abs(far_d[1][2][0]-far_d[2][2][0])*abs(far_d[1][2][0]-far_d[2][2][0])+abs(far_d[1][2][1]-far_d[2][2][1])*abs(far_d[1][2][1]-far_d[2][2][1]))
                dist_2_3=math.sqrt(abs(far_d[3][2][0]-far_d[2][2][0])*abs(far_d[3][2][0]-far_d[2][2][0])+abs(far_d[3][2][1]-far_d[2][2][1])*abs(far_d[3][2][1]-far_d[2][2][1]))
                thumb_change=0
                if dist_0_3>dist_0_1 and dist_0_3>dist_2_3 and dist_0_3>dist_1_2:
                    if dist_0_1>dist_2_3:
                        center_thumb_d=(int(far_d[0][2][0]),int(far_d[0][2][1]))
                    else: #dist_2_3>dist_0_1:
                        center_thumb_d=(int(far_d[3][2][0]),int(far_d[3][2][1]))
                    center_high_d=(int((far_d[1][1][0]+far_d[2][0][0])/2),int((far_d[1][1][1]+far_d[2][0][1])/2))
                    thumb_change=1
                if dist_2_3>dist_0_3 and dist_2_3>dist_1_2 and dist_2_3>dist_0_1:
                    if dist_0_3>dist_1_2:
                        center_thumb_d=(int(far_d[3][2][0]),int(far_d[3][2][1]))
                    else: #dist_1_2>dist_0_3:
                        center_thumb_d=(int(far_d[2][2][0]),int(far_d[2][2][1]))
                    center_high_d=(int((far_d[1][0][0]+far_d[0][1][0])/2),int((far_d[1][0][1]+far_d[0][1][1])/2))
                    thumb_change=1
                if dist_0_1>dist_0_3 and dist_0_1>dist_1_2 and dist_0_1>dist_2_3:
                    if dist_0_3>dist_1_2:
                        center_thumb_d=(int(far_d[0][2][0]),int(far_d[0][2][1]))
                    else: #dist_1_2>dist_0_3:
                        center_thumb_d=(int(far_d[1][2][0]),int(far_d[1][2][1]))
                    center_high_d=(int((far_d[2][1][0]+far_d[3][0][0])/2),int((far_d[2][1][1]+far_d[3][0][1])/2))
                    thumb_change=1

                if thumb_change==1:
                    cv2.circle(rgb,(center_thumb_d),5,[0,127,255],-1)    
                    cv2.circle(rgb,(center_high_d),5,[255,127,0],-1)    
                                    
                    u_wektor=(center_high_d[0]-center_d[0],center_high_d[1]-center_d[1])
                    dlug_u_wektor=math.sqrt(u_wektor[0]*u_wektor[0]+u_wektor[1]*u_wektor[1])
                    #wsp=dl_aruco_hand/(2*dlug_u_wektor)
                    wsp=0.33
                    u2_wektor=(int(wsp*u_wektor[0]),int(wsp*u_wektor[1]))

                    if u2_wektor[1]!=0: 
                        center_top_d_x=center_d[0]+u2_wektor[0]
                        center_top_d_y=center_d[1]+u2_wektor[1]
                        center_top_d=(center_top_d_x,center_top_d_y)
                        
                        center_down_d_x=center_d[0]-u2_wektor[0]
                        center_down_d_y=center_d[1]-u2_wektor[1]
                        center_down_d=(center_down_d_x,center_down_d_y)

                        x=math.sqrt((u2_wektor[0]*u2_wektor[0])/(u2_wektor[1]*u2_wektor[1])+u2_wektor[0]*u2_wektor[0]+u2_wektor[1]*u2_wektor[1]-1)
                        y=((-1)*u2_wektor[0]*x)/u2_wektor[1]
                        v_wektor=(int(x),int(y))

                        #wyznaczanie rogow 'markera' dloni              
                        center_right_d_x=center_top_d[0]+v_wektor[0]
                        center_right_d_y=center_top_d[1]+v_wektor[1]
                        center_right_d=(center_right_d_x,center_right_d_y)
                   
                        center_left_d_x=center_top_d[0]-v_wektor[0]
                        center_left_d_y=center_top_d[1]-v_wektor[1]
                        center_left_d=(center_left_d_x,center_left_d_y)
                       
                        center_right2_d_x=center_down_d[0]+v_wektor[0]
                        center_right2_d_y=center_down_d[1]+v_wektor[1]
                        center_right2_d=(center_right2_d_x,center_right2_d_y)
                    
                        center_left2_d_x=center_down_d[0]-v_wektor[0]
                        center_left2_d_y=center_down_d[1]-v_wektor[1]
                        center_left2_d=(center_left2_d_x,center_left2_d_y)
           
                        sumaR=math.sqrt((center_thumb_d[0]-center_right_d[0])*(center_thumb_d[0]-center_right_d[0])+(center_thumb_d[1]-center_right_d[1])*(center_thumb_d[1]-center_right_d[1])) + math.sqrt((center_thumb_d[0]-center_right2_d[0])*(center_thumb_d[0]-center_right2_d[0])+(center_thumb_d[1]-center_right2_d[1])*(center_thumb_d[1]-center_right2_d[1]))
                        sumaL=math.sqrt((center_thumb_d[0]-center_left_d[0])*(center_thumb_d[0]-center_left_d[0])+(center_thumb_d[1]-center_left_d[1])*(center_thumb_d[1]-center_left_d[1])) + math.sqrt((center_thumb_d[0]-center_left2_d[0])*(center_thumb_d[0]-center_left2_d[0])+(center_thumb_d[1]-center_left2_d[1])*(center_thumb_d[1]-center_left2_d[1]))
                        if sumaL < sumaR:
                            pom=center_left_d
                            center_left_d=center_right_d
                            center_right_d=pom
                            pom=center_left2_d
                            center_left2_d=center_right2_d
                            center_right2_d=pom

                        #rysowanie rogow 'markera' dloni 
                        #cv2.circle(rgb,center_top_d,6,[0,0,0],-1)
                        #cv2.circle(rgb,center_down_d,6,[50,50,200],-1)
                        #cv2.circle(rgb,center_right_d,6,[200,200,200],-1)
                        #cv2.circle(rgb,center_left_d,6,[50,200,20],-1)
                        #cv2.circle(rgb,center_right2_d,6,[100,200,150],-1)
                        #cv2.circle(rgb,center_left2_d,6,[180,100,30],-1)

                        cv2.circle(depth,center_left_d,10,[0,0,0],0)
                        cv2.circle(depth,center_right_d,10,[0,0,0],0)
                        cv2.circle(depth,center_right2_d,10,[0,0,0],0)
                        cv2.circle(depth,center_left2_d,10,[0,0,0],0)
                    
                        #przypisanie uladu wspolrzednych do dloni z narysowaniem jego osi 
                        handCorners=[]
                        handCorners.insert(0,np.float32([[[center_left_d[0],center_left_d[1]],[center_right_d[0],center_right_d[1]],[center_right2_d[0],center_right2_d[1]],[center_left2_d[0],center_left2_d[1]]]]))
                        hand_rvec,hand_tvec,_=aruco.estimatePoseSingleMarkers(handCorners,0.072,cameraMatrix,distCoeffs)
                   
                        hand_rvec=np.array([hand_rvec[0][0][0],hand_rvec[0][0][1],hand_rvec[0][0][2]],dtype=float)
                        hand_tvec=np.array([hand_tvec[0][0][0],hand_tvec[0][0][1],hand_tvec[0][0][2]],dtype=float)
                        handCorners.clear()
                        aruco.drawAxis(depth,cameraMatrix,distCoeffs,hand_rvec,hand_tvec,HAND_AXIS_SIZE)
                        aruco.drawAxis(rgb,cameraMatrix,distCoeffs,hand_rvec,hand_tvec,HAND_AXIS_SIZE)

                        hand_tvec[0]= center_d_x
                        hand_tvec[1]= center_d_y
                        hand_tvec[2]= center_d_z
                        hand_change=1
            else:
                far_d.clear()
                    
        #śledzenie dloni
        if len(centerList_d)>10:
            tmpA=1
        if tmpA == 1 and len(centerList_d)>0 :
            centerList_d.pop()
        drawPoints(depth,centerList_d)
        drawPoints(rgb,centerList_d)

        #*************ZMIANA ZALEZNOSCI + WYSWIETLANIE**************#
        if aruco_change == 1 and hand_change == 1:
            change=1
            hand_lack=0
            cv2.putText(mainWindow,'New system detected!!', (15,150), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,0,0),2)
            cv2.putText(mainWindow,'Hand detected!!', (50,180), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,0),2)
        elif aruco_change == 1 and hand_change == 0:
            change=2
            cv2.putText(mainWindow,'New system detected!!', (15,150), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,0,0),2)
        elif aruco_change == 0 and hand_change == 1:
            change=3
            hand_lack=0
            cv2.putText(mainWindow,'Hand detected!!', (50,180), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,0),2)
        else:
            change=0

        #zerowanie pozycji dloni
        if hand_change==0 or change==0:
            hand_lack=hand_lack+1
            if hand_lack==30:
                tmp_R=np.array(['-','-','-'])
                tmp_T=np.array(['-','-','-'])
                hand_lack=0
        if change == 0:
            cv2.putText(mainWindow,'CAMERA', (90,60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,0,255),2)
        
        cv2.putText(mainWindow,'Reference system:', (25,30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),2)
        if change==1 or aruco_change == 1 and hand_change == 1: #przypadek 1
            main_A[:3,:3]=inv_R(main_rvec)
            main_A[:3,3:4]=inv_T(main_rvec,main_tvec)

            cv2.Rodrigues(hand_rvec,hand_R)
            hand_A[:3,:3]=hand_R
            hand_A[:3,3:4]=np.array([[hand_tvec[0]],[hand_tvec[1]],[hand_tvec[2]]],dtype=float)

            hand_A=np.dot(main_A,hand_A) #macierz pozycji dloni w aruco
            hand_R=hand_A[:3,:3]
            hand_T=hand_A[:3,3:4]
            cv2.Rodrigues(hand_R,hand_rvec)
            for j in range(0,3):
                hand_tvec[j]=hand_T[j]
            #przyblizenia
            tmp_R2==np.array([round(main_rvec[0],3),round(main_rvec[1],3),round(main_rvec[2],3)],dtype=float)
            tmp_T2==np.array([round(100*main_tvec[0],1),round(100*main_tvec[1],1),round(100*main_tvec[2],1)],dtype=float)
            tmp_R=np.array([round(hand_rvec[0],3),round(hand_rvec[1],3),round(hand_rvec[2],3)],dtype=float)
            tmp_T=np.array([round(100*hand_tvec[0],1),round(100*hand_tvec[1],1),round(100*hand_tvec[2],1)],dtype=float)

            #wyswietlanie w oknie glownym
            cv2.putText(mainWindow,'ARUCO - id '+str(draw_id), (50,60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,0,255),2)
            cv2.putText(mainWindow,'Camera position: ', (325,30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),2)
            cv2.putText(mainWindow,'R: x: '+str(tmp_R2[0])+' rad  y: '+str(tmp_R2[1])+' rad  z: '+str(tmp_R2[2])+' rad', (325,60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
            cv2.putText(mainWindow,'T: x: '+str(tmp_T2[0])+' cm     y: '+str(tmp_T2[1])+' cm     z: '+str(tmp_T2[2])+' cm ', (325,90), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
            
        if change==2 or aruco_change == 1 and hand_change == 0:  #przypadek 2
            main_R=inv_R(main_rvec)
            main_T=inv_T(main_rvec,main_tvec)
            cv2.Rodrigues(main_R,main_rvec)
            for j in range(0,3):
                main_tvec[j]=main_T[j][0]
            #przyblizenia
            tmp_R2=np.array([round(main_rvec[0],3),round(main_rvec[1],3),round(main_rvec[2],3)],dtype=float)
            tmp_T2=np.array([round(100*main_tvec[0],1),round(100*main_tvec[1],1),round(100*main_tvec[2],1)],dtype=float)
            #wyswietlanie w oknie glownym
            cv2.putText(mainWindow,'ARUCO - id '+str(draw_id), (50,60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,0,255),2)
            cv2.putText(mainWindow,'Camera position: ', (325,30), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),2)
            cv2.putText(mainWindow,'R: x: '+str(tmp_R2[0])+' rad  y: '+str(tmp_R2[1])+' rad  z: '+str(tmp_R2[2])+' rad', (325,60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
            cv2.putText(mainWindow,'T: x: '+str(tmp_T2[0])+' cm     y: '+str(tmp_T2[1])+' cm     z: '+str(tmp_T2[2])+' cm ', (325,90), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)

        if change==3 or aruco_change == 0 and hand_change == 1:  #przypadek 3
            #przyblizenia
            tmp_R=np.array([round(hand_rvec[0],3),round(hand_rvec[1],3),round(hand_rvec[2],3)],dtype=float)
            tmp_T=np.array([round(100*hand_tvec[0],1),round(100*hand_tvec[1],1),round(100*hand_tvec[2],1)],dtype=float)
            #wyswietlanie w oknie glownym
            cv2.putText(mainWindow,'CAMERA', (90,60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,0,255),2)

        cv2.putText(mainWindow,'Hand position: ', (50,270), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),2)
        cv2.putText(mainWindow,'R: ', (50,300), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
        cv2.putText(mainWindow,'x:  '+str(tmp_R[0])+' rad', (50,330), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
        cv2.putText(mainWindow,'y:  '+str(tmp_R[1])+' rad', (50,360), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
        cv2.putText(mainWindow,'z:  '+str(tmp_R[2])+' rad', (50,390), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
        cv2.putText(mainWindow,'T: ',(50,450), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
        cv2.putText(mainWindow,'x:  '+str(tmp_T[0])+' cm', (50,480), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
        cv2.putText(mainWindow,'y:  '+str(tmp_T[1])+' cm', (50,510), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
        cv2.putText(mainWindow,'z:  '+str(tmp_T[2])+' cm', (50,540), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255,255,255),1)
                    
        #*********WYSWIETLANIE KLATEK**************#
        depth_thresholding = np.hstack((depth,depth_thresh))
        #cv2.imshow('Depth + Thresholding',depth_thresholding)
        cv2.imshow('Depth + Thresholding',depth_thresh)

        mainWindow[120:600,320:960]=rgb
        cv2.imshow('Hand Detector',mainWindow)
        
        #wyjscie z pętli
        k=cv2.waitKey(5) & 0xFF
        if k==27:
            break

    freenect.sync_stop()
    cv2.destroyAllWindows()
